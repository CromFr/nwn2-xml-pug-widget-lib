# NWN2 XML Pug widget library

Pug is a language designed to easily generate complex XML data, which means it
can be used to generate NWN2 GUIs. This repository is a library of generic Pug
widgets / templates to quickly design beautiful NWN2 GUIs.

Pug can also be used to localize XMLs, or generate repetitive structures.

# Example

![widget-demo GUI](widget-demo-0.png)
![widget-demo GUI](widget-demo-1.png)
![widget-demo GUI](widget-demo-2.png)

- [Pug code](widget-demo.pug)
- [Generated XML code](widget-demo.xml)

## Demo setup
1. Copy the folder `widget-demo-module` into `Documents/Neverwinter Nights
   2/modules/`
2. Copy `widget-demo.xml`, `widget-demo.pug`, `nwn2-widget-lib.pug`,
   `build.bat`  files into Documents/Neverwinter Nights 2/override/ (putting
   the xml file into the override will aloow the game to re-load it when the
   file has changed)
3. Start the game (or restart it if it was already running when copying the
   XML file), and start the single player module `widget-demo-module`
4. Use the lever to open the GUI.
5. Play with `widget-demo.pug`'s content, re-generate XML file by launching
   `build.bat`. Switch to the game, close and re-open the GUI.


## Quick Pug introduction

You can find all PugJS documentation here: https://pugjs.org/language/attributes.html

#### Pug XML representation:
The simplest Pug example would look like this:
```js
UIScene(
	name="YOUR_SCENE_NAME"
	priority="SCENE_INGAME"
	x="ALIGN_CENTER" y="ALIGN_CENTER" width=100 height=100
	draggable="true" backoutkey="true" scriptloadable="true"
	idleexpiretime=0.1
)
UIPane(width=100 height=50)
    UIButton(x="ALIGN_RIGHT" y="ALIGN_TOP" style="STYLE_CLOSE_BUTTON" OnLeftClick="UIButton_Input_ScreenClose()")
    UIFrame(fill="dark_rock_tile.tga")
```
You shouldn't be lost if you know a bit of NWN2 XML ;)

#### Pug templates:
Pug also provides templates (with a `+`), that are replaced with more complex XML:
```js
+FancyFramedPane(width=100 height=100 capturemouseclicks="false" padding=10)
    UIButton(text="Hello" style="STYLE_CLOSE_BUTTON")
```
will be translated to:
```xml
<UIPane width="151" height="158" capturemouseclicks="false">
  <UIPane x="25" y="33" width="100" height="100" capturemouseclicks="false">
    <UIButton text="Hello" style="STYLE_CLOSE_BUTTON"/>
  </UIPane>
  <UIFrame x="15" y="23" width="620" height="420" fill="cloth_bg.tga" fillstyle="tile"></UIFrame>
  <UIFrame topleft="frame_main_d_TL.tga" topright="frame_main_t_TR.tga" bottomleft="frame_main_d_BL.tga" bottomright="frame_main_d_BR.tga" top="frame_main_T.tga" bottom="frame_main_B.tga" left="frame_main_L.tga" right="frame_main_R.tga" border="45"></UIFrame>
</UIPane>
```

There are many predefined templates, like:
- `+FancyFramedPane`, `+SimpleFramedPane`
- `+Separator`
- `+CancelButton`, `+CustomButton`, `+SquareIconButton`, ...
- `+TabBar`
- `+LabeledPane`, `+LabeledTextInputWithReset`
- `+SectionHeader`, `+Scrollable`, `+Collapsable`
- And More ! see [nwn2-widget-lib.pug](nwn2-widget-lib.pug). Lines like `mixin
  XXX()` defines templates you can reuse, and each one is documented.

#### Pug variables and loops:

Lines starting with `-` are lines of code that uses a language similar to javascript.
```js
- var name = "MyWindowName"
- var windowSize = [600, 500]

UIText(width=windowSize[0] height=windowSize[1] text=name)
```

You can use this code to repeat XML elements:
```js
- for (var i = 0; i < 5; i++)
    UIText(x=0 y=i * 25 width=100 height=20 text="Text example " + i)

- var toDisplay = [{"name": "AC", "value": 42}, {"name": "BAB", "value": 10}]
- for item, i in toDisplay
    UIText(x=0  y=i * 25 width=50 height=20 text=item.name)
    UIText(x=50 y=i * 25 width=50 height=20 text=item.value)
```

#### Pug conversion
There are multiple HTML to Pug converters online, but most of them don't work
very well with NWN2 XML, and the resulting XML needs some additional manual post-processing

For example, https://html2jade.org/ doesn't handle correctly self-closing tags
(`<UIFrame ... />`), and converts XML tags and attribute names to lowercase.
However it does handle somewhat correctly NWN2 broken XML values and can be
handy for converting small blocks of XML.


# How to use

## Requirements

- You must install https://github.com/pugjs/pug-cli
    + Install Node + NPM: https://nodejs.org/en/download/
    + Install `pug-cli` by running in a command line (powershell or cmd on
      Windows): `npm install -g pug-cli`

## Generate your XML

On Windows, you need to run this command in a `cmd` window to generate your
NWN2 GUI file:

```batch
pug --pretty your-pug-gui.pug -E xml
```
